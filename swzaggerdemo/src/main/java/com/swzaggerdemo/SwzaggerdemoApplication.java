package com.swzaggerdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SwzaggerdemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SwzaggerdemoApplication.class, args);
	}
}

package com.swzaggerdemo;

import java.nio.file.Path;
import java.nio.file.Paths;

import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import io.github.swagger2markup.Swagger2MarkupConverter;

@Controller
public class MyController {
	@Autowired
	ServletContext context;

	@GetMapping("convert")
	public void convert() {

		Path localSwaggerFile = Paths
				.get("C:/Users/pavan_gar1/Downloads/swzaggerdemo/swzaggerdemo/src/swagger/swagger.yaml");
		Path outputDirectory = Paths.get("C:/Users/pavan_gar1/Downloads/swzaggerdemo/swzaggerdemo/src/asciidoc");

		Swagger2MarkupConverter.from(localSwaggerFile).build().toFolder(outputDirectory);
		System.out.println("ok");

	}

}

package com.swzaggerdemo;

import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import io.github.swagger2markup.Swagger2MarkupConverter;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SwzaggerdemoApplicationTests {

	@Test
	public void contextLoads() {
	}
	
	@Test
	public void convert() {
		
		Path localSwaggerFile = Paths
				.get("C:/Users/pavan_gar1/Downloads/swzaggerdemo/swzaggerdemo/src/swagger/swagger.yaml");
		Path outputDirectory = Paths.get("C:/Users/pavan_gar1/Downloads/swzaggerdemo/swzaggerdemo/src/asciidoc");

		Swagger2MarkupConverter.from(localSwaggerFile).build().toFolder(outputDirectory);
	}

}
